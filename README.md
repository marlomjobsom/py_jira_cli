# pyJIRA CLI

It provides a CLI interface to interact with Jira server

The steps described in this document was based on [Ubuntu 20.04 LTS x64](https://releases.ubuntu.com/20.04/).
For any other operational system, please visit the web page of each requirement.

[[_TOC_]]

## Requirements
* [Poetry](https://python-poetry.org/)
* [Python 3.8](https://www.python.org/downloads/release/python-380/)

## Installation

```shell script
pip install --user poetry
poetry completions bash > /etc/bash_completion.d/poetry.bash-completion
```

## DEV Setup

```shell script
poetry install

# Activate virtualenv
poetry shell
```

## Usage

In this section, It is described a common usage of `pyjira`.
The elements presented here are applicable to any other Jira commands [Jira commands](#jira-commands).

### Parameters

`pyjira` expects to have the parameters needed to open the communication with the Jira instance.
The parameters can be given directly through CLI interface

```shell script
pyjira --jira_url URL --jira_user USER --jira_password PASSWORD
```

The parameters can be read from the system variables as well.
In this case, the parameters can be omitted from the command line.

```shell script
export JIRA_URL=URL
export JIRA_USER=USER
export JIRA_PASSWORD=PASSWORD
pyjira
```

In these two examples, the commands will do nothing, since we did not call any Jira commands.

The parameters of the `pyjira` commands can be seen as global parameters. They are available to the Jira commands.

#### Naming convention

The name of the system variables are similar to the name of the CLI parameters:
 * `--jira_url` became `JIRA_URL`
 * `--jira_user` became `JIRA_USER`
 * `--jira_password` became `JIRA_PASSWORD`

 The convention adopted to convert from CLI parameter to system variable is:
 1. Remove the two dashes from the beginning of the CLI parameter name
 2. Capitalize all letters
 3. Separate the words by underscore

### Jira commands

*Jira commands* are operations implemented in `pyjira` that do something with the given Jira instance.

Each Jira command has its own parameters, and it follows the same parameter mechanism presented in [Parameters](#parameters). That is, it can receives parameters from CLI or system environment.

**IMPORTANT**: A Jira command may fail due to the level permission level of the Jira credential given. Make sure to the credential given are allowed to perform the Jira command chosen.

### Listing Jira commands

To list the available Jira commands, we can call `pyjira --help`. It will display its parameters, and the available Jira commands.

```shell
pyjira --help
usage: py-jira-cli [-h] [--jira_url JIRA_URL] [--jira_user JIRA_USER] [--jira_password JIRA_PASSWORD]
                   {set_epic_link,dump_version_time_estimated,dump_sprint_worklog} ...

It provides a CLI interface to interact with Jira server

optional arguments:
  -h, --help            show this help message and exit
  --jira_url JIRA_URL   The URL to Jira server
  --jira_user JIRA_USER
                        The Jira server login user
  --jira_password JIRA_PASSWORD
                        The Jira server login user password

Jira commands:
  {set_epic_link,dump_version_time_estimated,dump_sprint_worklog}
    set_epic_link       Set Epic Link in Jira unit works
    dump_version_time_estimated
                        It dumps from Jira the time estimated for a given version
    dump_sprint_worklog
                        It dumps from Jira the effort applied for a given sprint
```

We can show the help message of a Jira command by calling it directly `pyjira dump_version_time_estimated --help`.

Note that, in the example below, it does not list the parameters to access a Jira instance, since those parameters belongs to the `pyjira` command.

Here, it lists only the parameters that belongs to the subroutine `dump_version_time_estimated`. However, as we mentioned in [Passing parameters](#passing-parameters), the global parameters are available to this subroutine as well.

```shell
pyjira dump_version_time_estimated --help
usage: py-jira-cli dump_version_time_estimated [-h] --version VERSION [--dump_folder DUMP_FOLDER]
                                               [--gc_service_account_file_path GC_SERVICE_ACCOUNT_FILE_PATH]
                                               [--spreadsheet_id SPREADSHEET_ID]

It dumps from Jira the time estimated for a given version

optional arguments:
  -h, --help            show this help message and exit
  --version VERSION     The Jira version
  --dump_folder DUMP_FOLDER
                        The folder path where the Jira dump will be store
  --gc_service_account_file_path GC_SERVICE_ACCOUNT_FILE_PATH
                        The Google Cloud service account to authenticate on Google Sheets API
  --spreadsheet_id SPREADSHEET_ID
                        The Google Spreadsheet ID where the data will be uploaded to
```

### Executing a Jira command

In this section, we will illustrate how we can execute a Jira command. In this particular example, we will call the `dump_version_time_estimated`.

The parameters must succeed the operation.

 ```shell script
pyjira \
    --jira_url https://jira.url.com/ \
    --jira_user user \
    --jira_password password \
    dump_version_time_estimated \
    --version jira_version \
    --dump_folder dump_folder \
    --gc_service_account_file_path path_to_google_service_account_file \
    --spreadsheet_id google_spreadsheet_id
 ```

As we mention before, the parameters can be system variables as well. In this case, they can be omitted from CLI.

```shell script
export JIRA_URL=https://jira.url.com/
export JIRA_USER=user
export JIRA_PASSWORD=password
export VERSION=version
export DUMP_FOLDER=dump_folder
export GC_SERVICE_ACCOUNT_FILE_PATH=service_account_file_path
export SPREADSHEET_ID=spreadsheet_id
pyjira dump_version_time_estimated
```

**NOTE**: Be careful when using system variables. They can conflict with existing ones, and you may want to vary its value when call a Jira command.
