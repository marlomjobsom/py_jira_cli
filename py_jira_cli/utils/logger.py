# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Logger"""

import logging
import sys


class Logger(logging.Logger):
    """Logger"""

    def __init__(self, name='root'):
        """
        :param str name:
        """
        super().__init__(name=name, level=logging.INFO)
        formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(funcName)s:%(message)s')
        self._add_console_stream_handler(formatter)

    def _add_console_stream_handler(self, formatter):
        """
        :param logging.Formatter formatter:
        """
        console_stream_handler = logging.StreamHandler(sys.stdout)
        console_stream_handler.setLevel(logging.INFO)
        console_stream_handler.setFormatter(formatter)
        super().addHandler(console_stream_handler)
