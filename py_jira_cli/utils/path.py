# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Handles files and folders manipulation"""

import os

from py_jira_cli.utils.moment import Moment


class Path:
    """Handles files and folders manipulation"""

    @staticmethod
    def mk_now_dump_folder_path(path=None):
        """
        :param str path:
        :return str:
        """
        if not path:
            path = '.'

        folder_name = Moment.get_now('%Y-%m-%d-%H-%M-%S')
        folder_path = os.path.join(path, 'pyjira', 'dumps', folder_name)
        os.makedirs(folder_path, exist_ok=True)
        return folder_path
