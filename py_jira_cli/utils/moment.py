# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Handles date/time manipulation"""

from datetime import datetime


class Moment:
    """Handles date/time manipulation"""

    @staticmethod
    def jira_version_date_to_datetime(version_date):
        """
        :param str version_date:
        :return datetime:
        """
        return datetime.strptime(version_date, '%Y-%m-%d')

    @staticmethod
    def jira_date_to_datetime(issue_date):
        """
        :param issue_date:
        :return datetime:
        """
        return datetime.strptime(issue_date, '%Y-%m-%dT%H:%M:%S.%f%z')

    @staticmethod
    def get_now(str_from_time=None):
        """
        :param str str_from_time:
        :return datetime|str:
        """
        now = datetime.utcnow()

        if str_from_time:
            now = now.strftime(str_from_time)

        return now
