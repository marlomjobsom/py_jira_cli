# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""It makes an "required" argument to not be required when reading from system variables."""

import argparse

from decouple import config


class DecoupleArgparseAction(argparse.Action):
    """It makes a "required" argument to not be required when reading from system variables."""

    def __init__(self, env_name, required=True, default=None, **kwargs):
        """
        :param str env_name:
        :param bool required:
        :param str default:
        :param dict kwargs:
        """
        self._force_init_decouple()

        if not default and env_name:
            if env_name in config.config.repository.data:
                default = config(env_name)

        if required and default:
            required = False

        super().__init__(default=default, required=required, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, values)

    @classmethod
    def _force_init_decouple(cls):
        try:
            config()
        except TypeError:
            pass
