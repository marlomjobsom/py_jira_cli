# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Retrieves parameters from CLI interface"""

import argparse

from py_jira_cli.core.cli.commands.dump_sprint_worklog import DumpSprintWorklog
from py_jira_cli.core.cli.commands.dump_version_time_estimated import DumpVersionTimeEstimated
from py_jira_cli.core.cli.commands.set_epic_link import SetEpicLink
from py_jira_cli.core.cli.decouple_argparse_action import DecoupleArgparseAction


class CLI:
    """Retrieves parameters from CLI interface"""

    @classmethod
    def get_args(cls):
        """
        :return argparse.Namespace:
        """
        root_parser = cls._build_root_parser()
        cls._register_jira_commands(root_parser)
        args, _ = root_parser.parse_known_args()
        return args

    @classmethod
    def _build_root_parser(cls):
        """
        :return argparse.ArgumentParser:
        """
        root_parser = argparse.ArgumentParser(
            prog='py-jira-cli',
            description='It provides a CLI interface to interact with Jira server')
        root_parser.add_argument(
            '--jira_url',
            type=str, action=DecoupleArgparseAction,
            env_name='JIRA_URL', help='The URL to Jira server')
        root_parser.add_argument(
            '--jira_user',
            type=str, action=DecoupleArgparseAction,
            env_name='JIRA_USER', help='The Jira server login user')
        root_parser.add_argument(
            '--jira_password',
            type=str, action=DecoupleArgparseAction,
            env_name='JIRA_PASSWORD', help='The Jira server login user password')
        return root_parser

    @classmethod
    def _register_jira_commands(cls, root_parser):
        """
        :param argparse.ArgumentParser root_parser:
        """
        extra_args = [
            cls._add_dump_folder_arg_call_back,
            cls._add_gc_service_account_file_path_arg_call_back,
            cls._add_spreadsheet_id_arg_call_back]
        jira_sub_parser = root_parser.add_subparsers(title='Jira commands')
        DumpVersionTimeEstimated().register_cli_parser(jira_sub_parser, extra_args)
        DumpSprintWorklog().register_cli_parser(jira_sub_parser, extra_args)
        SetEpicLink().register_cli_parser(jira_sub_parser)

    @classmethod
    def _add_dump_folder_arg_call_back(cls, parser):
        """
        :param argparse.ArgumentParser parser:
        """
        help_dump_folder = 'The folder path where the Jira dump will be stored'
        parser.add_argument(
            '--dump_folder', type=str, action=DecoupleArgparseAction,
            env_name='DUMP_FOLDER', help=help_dump_folder)

    @classmethod
    def _add_gc_service_account_file_path_arg_call_back(cls, parser):
        """
        :param argparse.ArgumentParser parser:
        """
        help_gc_service_account_file_path = 'The Google Cloud service account to authenticate on Google Sheets API'
        parser.add_argument(
            '--gc_service_account_file_path', type=str, action=DecoupleArgparseAction,
            env_name='GC_SERVICE_ACCOUNT_FILE_PATH', required=False, help=help_gc_service_account_file_path)

    @classmethod
    def _add_spreadsheet_id_arg_call_back(cls, parser):
        """
        :param argparse.ArgumentParser parser:
        """
        help_spreadsheet_id = 'The Google Spreadsheet ID where the data will be uploaded to'
        parser.add_argument(
            '--spreadsheet_id', type=str, action=DecoupleArgparseAction,
            env_name='SPREADSHEET_ID', required=False, help=help_spreadsheet_id)
