# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""It structures a command/operation that will be triggered when receive CLI arguments"""

from abc import ABC

from py_jira_cli.utils.logger import Logger


class AbstractCommand(ABC):
    """It structures a command/operation that will be triggered when receive CLI arguments"""

    def __init__(self):
        self.cmd_name = type(self).__name__
        self.logger = Logger(name=self.cmd_name)

    def register_cli_parser(self, sub_parser, extra_args_call_backs=None):
        """
        :param argparse._SubParsersAction sub_parser:
        :param list[method] extra_args_call_backs:
        """
        raise NotImplementedError()

    def build_cli_parser(self, name, sub_parser, extra_args_call_backs):
        """
        :param str name:
        :param argparse._SubParsersAction sub_parser:
        :param list[method] extra_args_call_backs:
        :return argparse.ArgumentParser:
        """
        parser = sub_parser.add_parser(name=name)
        parser.set_defaults(func=self.run)
        self._add_extra_args(parser, extra_args_call_backs)
        return parser

    def setup(self, args):
        """
        :param argparse.Namespace args:
        """
        raise NotImplementedError()

    def operation(self, args):
        """
        :param argparse.Namespace args:
        """
        raise NotImplementedError()

    def teardown(self, args):
        """
        :param argparse.Namespace args:
        """
        raise NotImplementedError()

    def run(self, args):
        """
        :param argparse.Namespace args:
        """
        self.logger.info('Start "{}"'.format(self.cmd_name))
        self.setup(args)
        self.operation(args)
        self.teardown(args)
        self.logger.info('Finish "{}"'.format(self.cmd_name))

    @classmethod
    def _add_extra_args(cls, parser, extra_args_call_backs):
        """
        :param argparse.ArgumentParser:
        :param list[method] extra_args_call_backs:
        """
        if extra_args_call_backs:
            for extra_arg_call_back in extra_args_call_backs:
                extra_arg_call_back(parser)
