# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""It dumps from Jira the time estimated for a given version"""

from py_jira_cli.core.cli.commands.abstract_command import AbstractCommand
from py_jira_cli.core.cli.decouple_argparse_action import DecoupleArgparseAction
from py_jira_cli.core.jira_server.jira_server import JiraServer
from py_jira_cli.core.reporters.csv_reporter import CSVReporter
from py_jira_cli.core.reporters.google_sheet_reporter import GoogleSheetReporter
from py_jira_cli.utils.path import Path


class DumpVersionTimeEstimated(AbstractCommand):
    """It dumps from Jira the time estimated for a given version"""

    def __init__(self):
        super().__init__()
        self.jira = None
        self.jira_unit_works = None

    def register_cli_parser(self, sub_parser, extra_args_call_backs=None):
        """
        :param argparse._SubParsersAction sub_parser:
        :param list[method] extra_args_call_backs:
        """
        parser = super().build_cli_parser('dump_version_time_estimated', sub_parser, extra_args_call_backs)
        description = 'It dumps from Jira the time estimated for a given version'
        parser.help = description
        parser.description = description
        parser.add_argument(
            '--version', type=str, action=DecoupleArgparseAction, env_name='VERSION',
            required=True, help='The Jira version')

    def setup(self, args):
        """
        :param argparse.Namespace args:
        """
        args.jql = 'fixVersion = {}'.format(args.version)
        self.jira = JiraServer(args.jira_url, args.jira_user, args.jira_password)

    def operation(self, args):
        """
        :param argparse.Namespace args:
        """
        self.jira_unit_works = self.jira.search_jira_unit_work(args.jql)

    def teardown(self, args):
        """
        :param argparse.Namespace args:
        """
        if args.dump_folder:
            jira_unit_works_csv = self._csv_report(args)

            if args.gc_service_account_file_path and args.spreadsheet_id:
                self._google_sheet_report(args, jira_unit_works_csv)

    def _csv_report(self, args):
        """
        :param argparse.Namespace args:
        :return str:
        """
        dump_folder_path = Path.mk_now_dump_folder_path(args.dump_folder)
        return CSVReporter.write_csv_file(self.jira_unit_works, args.jql, dump_folder_path)

    @classmethod
    def _google_sheet_report(cls, args, csv_file_path):
        """
        :param argparse.Namespace args:
        :param str csv_file_path:
        """
        google_sheet_reporter = GoogleSheetReporter(args.gc_service_account_file_path, args.spreadsheet_id)
        google_sheet_reporter.upload_csv_data(args.version, csv_file_path)
