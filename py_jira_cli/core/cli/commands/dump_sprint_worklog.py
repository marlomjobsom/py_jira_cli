# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""It dumps from Jira the effort applied for a given sprint"""

from py_jira_cli.core.cli.commands.abstract_command import AbstractCommand
from py_jira_cli.core.cli.decouple_argparse_action import DecoupleArgparseAction
from py_jira_cli.core.jira_server.jira_server import JiraServer
from py_jira_cli.core.reporters.csv_reporter import CSVReporter
from py_jira_cli.core.reporters.google_sheet_reporter import GoogleSheetReporter
from py_jira_cli.utils.path import Path


class DumpSprintWorklog(AbstractCommand):
    """It dumps from Jira the effort applied for a given sprint"""

    def __init__(self):
        super().__init__()
        self.jira = None
        self.jira_unit_works = None

    def register_cli_parser(self, sub_parser, extra_args_call_backs=None):
        """
        :param argparse._SubParsersAction sub_parser:
        :param list[method] extra_args_call_backs:
        """
        parser = super().build_cli_parser('dump_sprint_worklog', sub_parser, extra_args_call_backs)
        description = 'It dumps from Jira the effort applied for a given sprint'
        parser.help = description
        parser.description = description
        parser.add_argument(
            '--sprint', type=str, action=DecoupleArgparseAction, env_name='SPRINT',
            required=True, help='The Jira sprint')

    def setup(self, args):
        """
        :param argparse.Namespace args:
        """
        args.jql = 'Sprint = {}'.format(args.sprint)
        self.jira = JiraServer(args.jira_url, args.jira_user, args.jira_password)

    def operation(self, args):
        """
        :param argparse.Namespace args:
        """
        self.jira_unit_works = self.jira.search_jira_unit_work(args.jql)
        self.jira.load_jira_unit_work_worklog(self.jira_unit_works)

    def teardown(self, args):
        """
        :param argparse.Namespace args:
        """
        if args.dump_folder:
            jira_unit_works_csv = self._csv_report(args)

            if args.gc_service_account_file_path and args.spreadsheet_id:
                self._google_sheet_report(args, jira_unit_works_csv)

    def _csv_report(self, args):
        """
        :param argparse.Namespace args:
        :return str:
        """
        dump_folder_path = Path.mk_now_dump_folder_path(args.dump_folder)
        return CSVReporter.write_csv_file(self.jira_unit_works, args.jql, dump_folder_path)

    @classmethod
    def _google_sheet_report(cls, args, csv_file_path):
        """
        :param argparse.Namespace args:
        :param str csv_file_path:
        """
        google_sheet_reporter = GoogleSheetReporter(args.gc_service_account_file_path, args.spreadsheet_id)
        google_sheet_reporter.upload_csv_data(args.sprint, csv_file_path)
