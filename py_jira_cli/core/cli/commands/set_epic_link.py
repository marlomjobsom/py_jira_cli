# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""It updates the Epic Link field of the jira unit works found to the new epic link key given"""

from py_jira_cli.core.cli.commands.abstract_command import AbstractCommand
from py_jira_cli.core.cli.decouple_argparse_action import DecoupleArgparseAction
from py_jira_cli.core.jira_server.jira_server import JiraServer


class SetEpicLink(AbstractCommand):
    """It updates the Epic Link field of the jira unit works found to the new epic link key given"""

    def __init__(self):
        super().__init__()
        self.jira = None

    def register_cli_parser(self, sub_parser, extra_args_call_backs=None):
        """
        :param argparse._SubParsersAction sub_parser:
        :param list[method] extra_args_call_backs:
        """
        parser = super().build_cli_parser('set_epic_link', sub_parser, extra_args_call_backs)
        description = 'Set Epic Link in Jira unit works'
        parser.help = description
        parser.description = description
        parser.add_argument(
            '--epic_key', type=str, action=DecoupleArgparseAction, env_name='EPIC_KEY',
            required=True, help='The Jira Epic key')
        parser.add_argument(
            '--jql', type=str, action=DecoupleArgparseAction, env_name='JQL',
            required=True, help='The JQL to retrieve the Jira issues to have their Epic Link updated')

    def setup(self, args):
        """
        :param argparse.Namespace args:
        """
        self.jira = JiraServer(args.jira_url, args.jira_user, args.jira_password)

    def operation(self, args):
        """
        :param argparse.Namespace args:
        """
        self.jira.set_epic_link(args.jql, args.epic_key)

    def teardown(self, args):
        pass
