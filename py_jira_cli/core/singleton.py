#!/usr/bin/env python
# -*-coding: utf-8-*-

"""A singleton base class"""


class Singleton(type):
    """A singleton base class"""

    _instances = dict()

    def __call__(cls, *args, **kwargs):
        """
        :param list args:
        :param dict kwargs:
        :return class:
        """
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)

        return cls._instances[cls]
