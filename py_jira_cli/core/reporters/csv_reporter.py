# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Export Jira data in .CSV file"""

import codecs
import csv
import os

from py_jira_cli.utils.logger import Logger


class CSVReporter:
    """Export Jira data in .CSV file"""

    logger = Logger()

    @classmethod
    def write_csv_file(cls, issues, jql, folder):
        """
        :param list[py_jira_cli.core.jira_server.beans.abstract_jira_issue.AbstractJiraIssue] issues:
        :param str jql:
        :param str folder:
        :return str:
        """
        file_path = CSVReporter._get_file_path(issues[0], folder)
        header = CSVReporter._get_header(issues[0], jql)
        cls.logger.info('Saving #{} {}'.format(len(issues), str(issues[0])))

        with codecs.open(filename=file_path, mode='w', encoding='utf-8') as _file:
            csv_writer = csv.DictWriter(_file, delimiter=',', fieldnames=header)
            csv_writer.writeheader()

            for jira_issue in issues:
                csv_data = jira_issue.build_csv_data()

                for entry in csv_data:
                    csv_writer.writerow(entry)

        return file_path

    @classmethod
    def _get_file_path(cls, issue, folder):
        """
        :param py_jira_cli.core.jira_server.beans.abstract_jira_issue.AbstractJiraIssue issue:
        :param str folder:
        :return str:
        """
        file_name = '{}.csv'.format(str(issue).lower())
        file_path = os.path.join(folder, file_name)
        cls.logger.info('{} file path: {}'.format(str(issue), file_path))
        return file_path

    @classmethod
    def _get_header(cls, issue, jql):
        """
        :param py_jira_cli.core.jira_server.beans.abstract_jira_issue.AbstractJiraIssue issue:
        :param str jql:
        :return list:
        """
        csv_data = issue.build_csv_data()[0]
        header = list(csv_data.keys())
        header.append(jql)
        cls.logger.info('{} CSV header: {}'.format(str(issue), list(header)))
        return header
