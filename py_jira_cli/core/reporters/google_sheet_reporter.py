# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Access layer to Google Sheet APIs"""

import csv

import gspread
from oauth2client.service_account import ServiceAccountCredentials

from py_jira_cli.utils.logger import Logger


class GoogleSheetReporter:
    """Access layer to Google Sheet APIs"""

    logger = Logger()

    def __init__(self, service_account_file_path, spreadsheet_id):
        """
        :param str service_account_file_path:
        :param str spreadsheet_id:
        """
        super().__init__()
        gspread_client = self._auth_gspread(service_account_file_path)

        self.logger.info('Opening sheet id "{}"'.format(spreadsheet_id))
        self._spreadsheet = gspread_client.open_by_key(spreadsheet_id)
        self.logger.info('Google sheet retrieved "{}"'.format(self._spreadsheet))

    def upload_csv_data(self, worksheet_name, csv_file_path):
        """
        :param worksheet_name:
        :param csv_file_path:
        :return:
        """
        params = {'valueInputOption': 'USER_ENTERED'}

        with open(csv_file_path) as _file:
            csv_reader = csv.reader(_file)
            body = dict(values=list(csv_reader))

            msg = 'Uploading CSV "{}" into Worksheet "{}"'.format(csv_file_path, worksheet_name)
            self.logger.info(msg)

            self._creates_worksheet(worksheet_name)
            self._spreadsheet.values_update(worksheet_name, params=params, body=body)

    def _creates_worksheet(self, worksheet_name):
        """
        :param worksheet_name:
        """
        try:
            self._spreadsheet.worksheet(worksheet_name)
        except gspread.exceptions.WorksheetNotFound:
            self.logger.info('Creating the worksheet "{}"'.format(worksheet_name))
            self._spreadsheet.add_worksheet(worksheet_name, 1, 1)

    @classmethod
    def _auth_gspread(cls, service_account_file_path):
        """
        :param str service_account_file_path:
        :return gspread.client.Client:
        """
        scope = ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive']
        credentials = ServiceAccountCredentials.from_json_keyfile_name(service_account_file_path, scope)
        return gspread.authorize(credentials)
