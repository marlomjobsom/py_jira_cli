# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Access layer to Jira server"""

from jira import JIRA

from py_jira_cli.core.jira_server.beans.jira_unit_work import JiraUnitWork
from py_jira_cli.core.singleton import Singleton
from py_jira_cli.utils.logger import Logger
from py_jira_cli.utils.moment import Moment


class JiraServer(metaclass=Singleton):
    """Access layer to Jira server"""

    def __init__(self, jira_url, jira_user, jira_password):
        """
        :param str jira_url:
        :param str jira_user:
        :param str jira_password:
        """
        super().__init__()
        self._logger = Logger(name=str(self))
        self._instance = JIRA(
            server=jira_url,
            basic_auth=(jira_user, jira_password),
            validate=True,
            get_server_info=True)

    def search_jira_unit_work(self, jql):
        """
        :param str jql:
        :return list[JiraUnitWork]:
        """
        jira_tasks = list()
        issues = self._perform_search_issues(jql, JiraUnitWork.get_jira_fields())

        for issue in issues:
            jira_task = JiraUnitWork(issue)
            jira_tasks.append(jira_task)

        return jira_tasks

    def load_jira_unit_work_worklog(self, jira_unit_works):
        """
        :param list[JiraUnitWork] jira_unit_works:
        """
        self._logger.info('Worklogs for #{} issues'.format(len(jira_unit_works)))
        start = Moment.get_now()
        len_worklogs = 0

        for jira_unit_work in jira_unit_works:
            issue_worklogs = self._instance.worklogs(jira_unit_work.key)
            len_worklogs += len(issue_worklogs)
            jira_unit_work.worklogs = issue_worklogs

        end = Moment.get_now()
        self._logger.info('Duration: {}'.format(end - start))

    def set_epic_link(self, jql, epic_key):
        """
        :param str jql:
        :param str epic_key:
        """
        jira_unit_work = self.search_jira_unit_work(jql)
        keys = [issue.key for issue in jira_unit_work]
        self._perform_add_issues_to_epic(epic_key, keys)

    def retrieve_total_entries(self, jql):
        """
        :param str jql:
        :return int:
        """
        response = self._instance._get_json('search', params={"jql": jql, "maxResults": 0})
        return response.get('total')

    def _perform_add_issues_to_epic(self, epic_key, issue_keys):
        """
        :param str epic_key:
        :param list[str] issue_keys:
        """
        self._logger.info('Setting Epic Key {} in {}'.format(epic_key, issue_keys))
        start = Moment.get_now()
        self._instance.add_issues_to_epic(epic_id=epic_key, issue_keys=issue_keys)
        end = Moment.get_now()
        self._logger.info('Duration: {}'.format(end - start))

    def _perform_search_issues(self, jql, fields=None):
        """
        :param str jql:
        :param list[str] fields:
        :return JqlResult:
        """
        self._logger.info('JQL: {}'.format(jql))
        fields = self._get_jira_fields_name_for_sql(fields)

        start = Moment.get_now()
        issues = list(self._instance.search_issues(jql_str=jql, maxResults=False, fields=fields))
        versions = self._perform_version(issues)
        self._set_issues_versions(issues, versions)
        end = Moment.get_now()
        self._logger.info('Duration: {}'.format(end - start))

        return issues

    def _perform_version(self, issues):
        """
        :param list[jira.resources.Issue] issues:
        :return list[jira.resources.Version]:
        """
        versions_ids = self._retrieve_versions_ids(issues)
        complete_versions = list()

        self._logger.info('Retrieving {} Versions'.format(len(versions_ids)))
        start = Moment.get_now()
        for version_id in versions_ids:
            complete_versions.append(self._instance.version(id=version_id))
        end = Moment.get_now()
        self._logger.info('Duration: {}'.format(end - start))

        return complete_versions

    @classmethod
    def _retrieve_versions_ids(cls, issues):
        """
        :param list[jira.resources.Issue] issues:
        :return set:
        """
        versions_lists = [issue.fields.fixVersions for issue in issues]
        versions_ids = set()

        for versions in versions_lists:
            if versions:
                for version in versions:
                    versions_ids.add(version.id)

        return versions_ids

    @classmethod
    def _set_issues_versions(cls, issues, versions):
        """
        :param list[jira.resources.Issue] issues:
        :return list[jira.resources.Version]:
        :return list[jira.resources.Issue]:
        """
        for issue in issues:
            fix_versions = issue.fields.fixVersions
            if fix_versions:
                complete_versions = list()

                for fix_version in fix_versions:
                    versions_found = [version for version in versions if fix_version.id == version.id]
                    complete_versions.extend(versions_found)

                issue.fields.fixVersions = complete_versions

    @classmethod
    def _get_jira_fields_name_for_sql(cls, jira_fields_names):
        """
        :param list[str] jira_fields_names:
        :return str:
        """
        if jira_fields_names:
            # IMPORTANT: It must not have any space after the comma
            fields = ','.join(jira_fields_names)
            return fields
