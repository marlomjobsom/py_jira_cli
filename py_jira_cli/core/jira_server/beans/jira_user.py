# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Jira server user"""

_UNASSIGNED = 'Unassigned'


class JiraUser:
    """Jira server user"""

    def __init__(self, user):
        """
        :param jira.resources.User user:
        """
        self._name = user.displayName.strip() if user else _UNASSIGNED
        self._email = user.emailAddress.strip() if user else _UNASSIGNED
        self._is_active = bool(user)

    @property
    def name(self):
        """
        :return str:
        """
        return self._name

    @property
    def email(self):
        """
        :return str:
        """
        return self._email

    @property
    def is_active(self):
        """
        :return bool:
        """
        return self._is_active
