# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""It is a Jira issue unit work"""

from py_jira_cli.core.jira_server.beans.abstract_jira_issue import AbstractJiraIssue
from py_jira_cli.core.jira_server.beans.jira_version import JiraVersion
from py_jira_cli.core.jira_server.beans.jira_worklog import JiraWorkLog
from py_jira_cli.utils.moment import Moment


class JiraUnitWork(AbstractJiraIssue):
    """It is a Jira issue unit work"""

    def __init__(self, issue, worklogs=None):
        """
        :param jira.resources.Issue issue:
        :param list[jira.resources.Worklog] worklogs:
        """
        super().__init__(issue)
        self._resolution_type = self._extract_resolution_type(issue)
        self._resolution_date = self._extract_resolution_date(issue)
        self._affected_versions = self._extract_versions(issue, 'versions')
        self._fix_versions = self._extract_versions(issue, 'fixVersions')
        self._time_estimated_in_sec = issue.fields.timeoriginalestimate
        self._worklogs = self._build_jira_worklogs(worklogs)

    @property
    def resolution_type(self):
        """
        :return str:
        """
        return self._resolution_type

    @property
    def resolution_date(self):
        """
        :return datetime.datetime:
        """
        return self._resolution_date

    @property
    def affected_versions(self):
        """
        :return list[JiraVersion]:
        """
        return self._affected_versions

    @property
    def fix_versions(self):
        """
        :return list[JiraVersion]:
        """
        return self._fix_versions

    @property
    def time_estimated_in_sec(self):
        """
        :return int:
        """
        return self._time_estimated_in_sec

    @property
    def worklogs(self):
        """
        :return list[JiraWorkLog]:
        """
        return self._worklogs

    @worklogs.setter
    def worklogs(self, worklogs):
        """
        :param list[jira.resources.Worklog]:
        """
        self._worklogs = self._build_jira_worklogs(worklogs)

    @classmethod
    def get_jira_fields(cls):
        """
        :return list:
        """
        common_fields = super().get_jira_fields()
        task_fields = ['resolution', 'resolutiondate', 'versions', 'fixVersions', 'timeoriginalestimate']
        return common_fields + task_fields

    @classmethod
    def get_expected_types(cls):
        """
        :return list:
        """
        return ['Story', 'Defect', 'Task']

    def build_csv_data(self):
        """
        :return list:
        """
        csv_data_entries = list()
        jira_unit_work_csv_data = self._build_jira_unit_work_csv_data_entry()

        if self._worklogs:
            self._append_worklogs_csv_data(jira_unit_work_csv_data, csv_data_entries)
        else:
            csv_data_entries.append(jira_unit_work_csv_data)

        return csv_data_entries

    @classmethod
    def _extract_resolution_type(cls, issue):
        """
        :param jira.resources.Issue issue:
        :return str:
        """
        if issue.fields.__dict__.get('resolution'):
            return issue.fields.resolution.name

    @classmethod
    def _extract_resolution_date(cls, issue):
        """
        :param jira.resources.Issue issue:
        :return datatime.datetime:
        """

        if issue.fields.__dict__.get('resolutiondate'):
            return Moment.jira_date_to_datetime(issue.fields.resolutiondate)

    @classmethod
    def _extract_versions(cls, issue, attr):
        """
        :param jira.resources.Issue issue:
        :param str attr:
        :return list[JiraVersion]:
        """
        versions = list()

        for version in issue.fields.__dict__.get(attr):
            versions.append(JiraVersion(version))

        return versions

    @classmethod
    def _build_jira_worklogs(cls, worklogs):
        """
        :param list[jira.resources.Worklog] worklogs:
        :return list[JiraWorkLog]:
        """
        if worklogs:
            jira_worklogs = list()

            for worklog in worklogs:
                jira_worklogs.append(JiraWorkLog(worklog))

            return jira_worklogs

    def _build_jira_unit_work_csv_data_entry(self):
        """
        :return dict:
        """
        csv_data_entry = super().build_csv_data()
        csv_data_entry.update({
            'resolution type': self._resolution_type,
            'resolution date': self._resolution_date,
            'affected version': self._affected_versions[0].name if self._affected_versions else None,
            'fix version': self._fix_versions[0].name if self._fix_versions else None,
            'fix version start date': self._fix_versions[0].start_date if self._fix_versions else None,
            'fix version release date': self._fix_versions[0].release_date if self._fix_versions else None,
            'time estimated in sec': self._time_estimated_in_sec})
        return csv_data_entry

    def _append_worklogs_csv_data(self, csv_data_entry, csv_data_entries):
        """
        :param dict csv_data_entry:
        :param list csv_data_entries:
        """
        for worklog in self._worklogs:
            # Register a new entry for each work-log
            copied_csv_data_entry = csv_data_entry.copy()
            copied_csv_data_entry.update({
                'logger': worklog.author_create.name,
                'log date': worklog.started,
                'time spent in sec': worklog.time_spent_in_sec})

            csv_data_entries.append(copied_csv_data_entry)
