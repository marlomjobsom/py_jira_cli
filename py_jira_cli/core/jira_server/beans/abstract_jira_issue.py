# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""An abstract Jira issue"""

from abc import ABC
from abc import abstractmethod

from py_jira_cli.core.jira_server.beans.jira_user import JiraUser
from py_jira_cli.utils.moment import Moment


class AbstractJiraIssue(ABC):
    """An abstract Jira issue"""

    def __init__(self, issue):
        """
        :param jira.resources.Issue issue:
        """
        self._key = issue.key
        self._created = Moment.jira_date_to_datetime(issue.fields.created)
        self._updated = Moment.jira_date_to_datetime(issue.fields.updated)
        self._status = issue.fields.status.name
        self._summary = issue.fields.summary
        self._assignee = JiraUser(issue.fields.assignee)
        self._issue_type = issue.fields.issuetype.name

    def __str__(self):
        """
        :return str:
        """
        return type(self).__name__

    @property
    def key(self):
        """
        :return str:
        """
        return self._key

    @property
    def created(self):
        """
        :return datetime.datetime:
        """
        return self._created

    @property
    def updated(self):
        """
        :return datetime.datetime:
        """
        return self._updated

    @property
    def status(self):
        """
        :return str:
        """
        return self._status

    @property
    def summary(self):
        """
        :return str:
        """
        return self._summary

    @property
    def assignee(self):
        """
        :return JiraUser:
        """
        return self._assignee

    @property
    def issue_type(self):
        """
        :return str:
        """
        return self._issue_type

    @abstractmethod
    def get_expected_types(self):
        """
        :return list:
        """
        pass

    @classmethod
    def get_jira_fields(cls):
        """
        :return list:
        """
        return ['created', 'updated', 'status', 'summary', 'assignee', 'issuetype']

    def build_csv_data(self):
        """
        :return dict:
        """
        return {
            'key': self._key,
            'created': self._created,
            'updated': self._updated,
            'status': self._status,
            'summary': self._summary,
            'assignee': self._assignee.name,
            'issue type': self._issue_type}
