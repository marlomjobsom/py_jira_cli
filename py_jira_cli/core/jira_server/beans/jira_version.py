# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""It groups Jira unit works for a particular software version"""

from py_jira_cli.utils.moment import Moment


class JiraVersion:
    """It groups Jira unit works for a particular software version"""

    def __init__(self, version):
        """
        :param jira.resources.Version version:
        """
        self._name = version.name
        self._start_date = self._extract_date(version, 'startDate')
        self._release_date = self._extract_date(version, 'releaseDate')

    @property
    def name(self):
        """
        :return str:
        """
        return self._name

    @property
    def start_date(self):
        """
        :return datetime.datetime:
        """
        return self._start_date

    @property
    def release_date(self):
        """
        :return datetime.datetime:
        """
        return self._release_date

    @classmethod
    def _extract_date(cls, version, attr):
        """
        :param jira.resources.Version version:
        :param str attr:
        :return datetime.datetime:
        """
        if version.__dict__.get(attr):
            return Moment.jira_version_date_to_datetime(version.__dict__.get(attr))
