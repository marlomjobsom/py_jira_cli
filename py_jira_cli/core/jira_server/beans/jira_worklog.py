# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Work-log of a Jira unit work"""

from py_jira_cli.core.jira_server.beans.jira_user import JiraUser
from py_jira_cli.utils.moment import Moment


class JiraWorkLog:
    """Work-log of a Jira unit work"""

    def __init__(self, worklog):
        """
        :param jira.resources.Worklog worklog:
        """
        self._author_create = JiraUser(worklog.author)
        self._started = Moment.jira_date_to_datetime(worklog.started)
        self._time_spent_in_sec = worklog.timeSpentSeconds

    @property
    def author_create(self):
        """
        :return JiraUser:
        """
        return self._author_create

    @property
    def started(self):
        """
        :return datetime.datetime:
        """
        return self._started

    @property
    def time_spent_in_sec(self):
        """
        :return int:
        """
        return self._time_spent_in_sec
