# !/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Entry-point"""

import sys

from py_jira_cli.core.cli.cli import CLI


def main():
    """Entry-point"""
    args = CLI.get_args()

    if 'func' in args:
        args.func(args)
    else:
        sys.exit('No Jira command chosen')


if __name__ == '__main__':
    main()
